/**
 * author: Enyonam
 * Date: 30/08/19
 */

import org.openqa.selenium.By;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.GoogleLandingPage;
import pages.SearchResultsPage;

import java.util.concurrent.TimeUnit;

public class GoogleSearchTest {
    WebDriver driver;
    GoogleLandingPage objLandingPage = new GoogleLandingPage();
    SearchResultsPage objResultsPage;
    public static WebDriverWait wait;
    public static ChromeOptions options;

    @BeforeSuite
    public void testSetup() {
        String pathToDriver = "src\\main\\resources\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", pathToDriver);//setup driver path

        options = new ChromeOptions();//TODO go through arguments

        options.addArguments("--disable-extensions");
        options.addArguments("disable-infobars");
        options.addArguments("test-type");
        options.addArguments("enable-strict-powerful-feature-restrictions");
        options.setCapability(ChromeOptions.CAPABILITY, options);
        options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE); //To remove warning INFO: Using `new ChromeOptions()` is preferred to `DesiredCapabilities.chrome()`

        driver = new ChromeDriver(options);//instantiate Chrome Driver
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 30);
    }

    @Test(priority = 0)
    public void goToHomePage() {
        objLandingPage = new GoogleLandingPage(driver);
        //navigate to landing page
        driver.get("https://www.google.com");//open url.
        //validate page
//        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("logo-default")));
        //identifying search bar and type in Samuel Eto'o
        driver.findElement(By.xpath("//input[@name='q']")).sendKeys("Samuel Eto'o");
        //Identifying search button and clicking it to run search
        driver.findElement(By.xpath("(//input[@value='Google Search'])[2]")).click();
    }

    @Test(priority = 1)
    public void searchResultsVerification() {
        objResultsPage = new SearchResultsPage(); //TODO MY COMMENT
        //validate search was successful
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(), \"Samuel Eto'o - Wikipedia\")]")));
        //close browser
        driver.close();
    }
}
