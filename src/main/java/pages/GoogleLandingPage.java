package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GoogleLandingPage {
    WebDriver driver;
    By pageVerification =By.id("logo-default");
    By searchBarElement = By.id("fakebox-input");
    By searchButton = By.xpath("(//input[@value='Google Search'])[2]");

    public GoogleLandingPage(WebDriver driver) {
    }

    public GoogleLandingPage() {

    }


//    public void verifyPage(){
//     driver.findElement(pageVerification);
//    }

    public By verifyPage(){
        return pageVerification;
    }
    public void identifySearchBar(){
     driver.findElement(searchBarElement);
    }
    public void identifySearchButton(){
        driver.findElement(searchButton);
    }
    public void clickLogin(){
        driver.findElement(searchButton).click();
    }
}
