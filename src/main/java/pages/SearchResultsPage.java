package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SearchResultsPage {
    WebDriver driver;
    By searchResultVerification = By.xpath("//div[contains(text(), \"Samuel Eto'o - Wikipedia\")]");

    public void searchResultsPage (WebDriver driver){
        this.driver = driver;
    }
    //Get value to authenticate page
    public void authenticatePage(){
        driver.findElement(searchResultVerification);
    }
}
